# README

Melvin MVP, playing with native API's to record audio.

The audio file is sent to an AWS Lambda function which in turn uses Google Speech to Text api to transcribe the data.

The text is returned to the client and displayed to the user.

- 0.0.10

### How do I get set up?

- Clone repo
- npm install
- npm start

### Contribution guidelines

- Pull Requests are a requirement

### Who do I talk to?

- JamadanMan
