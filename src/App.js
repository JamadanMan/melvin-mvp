import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import BenzAmrRecorder from 'benz-amr-recorder';

class App extends Component {
  constructor() {
    super();

    this.amrRec = new BenzAmrRecorder();

    this.state = {
      isRecording: false,
      isTranscribing: false,
    };
  }

  startRecording() {
    console.log('start-clicked');
    navigator.getUserMedia =
      navigator.getUserMedia || navigator.webkitGetUserMedia;
    navigator.getUserMedia(
      { audio: true },
      () => {
        if (!this.amrRec.isInit()) {
          this.amrRec.initWithRecord().then(() => {
            this.amrRec.startRecord();
          });
        } else {
          this.amrRec.startRecord();
        }

        this.setState({ isRecording: true });
      },
      () => {
        console.log('failed to get microphone');
      }
    );
  }

  stopRecording() {
    console.log('stop-clicked');
    this.amrRec.finishRecord().then(() => {
      this.setState({ isRecording: false });
    });
  }

  async gRequest(data) {
    const postObject = {
      data,
    };

    const url =
      'https://7llrajxruk.execute-api.eu-west-1.amazonaws.com/prod/getTranscript';

    const response = await fetch(url, {
      body: JSON.stringify(postObject), // must match 'Content-Type' header
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      headers: {
        'content-type': 'application/json',
        'x-api-key': 'PomB1cCwzw8MCskeksWNu1x9aPyKNfSc4utG47To',
      },
      method: 'POST', // *GET, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *same-origin
      referrer: 'no-referrer', // *client
    }).then(async response => {
      const res = await response.json();
      return res;
    });

    return response;
  }

  async transcribe() {
    this.setState({ isTranscribing: true });
    const file = this.amrRec.getBlob();
    let base64data = '';

    var reader = new FileReader();
    reader.onloadend = async () => {
      const result = reader.result;
      base64data = result.substr(result.indexOf(',') + 1);
      console.log('base64 ->>>>>', base64data);
      const response = await this.gRequest(base64data);
      this.setState({
        transcripts: response.results[0].alternatives[0].transcript,
        isTranscribing: false,
      });
    };
    reader.readAsDataURL(file);
  }

  downloadAudio() {
    const file = this.amrRec.getBlob();
    if (!!file) {
      window.location.href = window.URL.createObjectURL(file);
    }

    // if (!!this.state.transcripts) {
    //   let element = document.createElement('a');
    //   element.setAttribute(
    //     'href',
    //     'data:text/plain;charset=utf-8,' +
    //       encodeURIComponent(JSON.stringify(this.state.transcripts, null, 4))
    //   );
    //   element.setAttribute('download', 'transcript.txt');

    //   element.style.display = 'none';
    //   document.body.appendChild(element);

    //   element.click();
    //   document.body.removeChild(element);
    // }

    this.amrRec = new BenzAmrRecorder();
    this.amrRec.initWithRecord();
    this.setState({
      transcripts: undefined,
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Melvin</h1>
        </header>
        <div className="App-intro">
          {this.state.isRecording && (
            <div className="stop" onClick={this.stopRecording.bind(this)}>
              STOP
            </div>
          )}
          {!this.state.isRecording && (
            <div className="start" onClick={this.startRecording.bind(this)}>
              START
            </div>
          )}
          {this.state.transcripts && (
            <div>
              DATA
              <br />
              {JSON.stringify(this.state.transcripts, null, 4)}
            </div>
          )}
          {this.state.isTranscribing && (
            <div>
              LOADING
              <img src={logo} className="App-logo" alt="logo" />
            </div>
          )}
          <br />
          <br />
          {this.amrRec.getBlob() && (
            <div>
              <div onClick={this.transcribe.bind(this)}>TRANSCRIBE</div>

              <br />
              <br />
              <div onClick={this.downloadAudio.bind(this)}>DOWNLOAD</div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default App;
